using System;
using System.Diagnostics;
using Godot;

public class LevelExit : Area2D
{
    private void OnBodyEntered(object body)
    {
        if (!(body is Player)) return;

        var gameState = GameState.GetInstance(this);
        var level = GetParent<Level>();
        gameState.StopAllGlobalDisasters(level);

        GetTree().ChangeScene("ui/Statistics.tscn");
    }
}
