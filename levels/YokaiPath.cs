using Godot;
using System;

public class YokaiPath : PathFollow2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

    public override void _Process(float delta) {
   
        Offset += 300 * delta;
        var yokai = GetNode<Sprite>("Yokai");
        if (yokai.GlobalPosition.x < 5500) {
            yokai.FlipV = false; 
        }
        if (yokai.GlobalPosition.x > 9000) {
            yokai.FlipV = true; 
        }
        
    }
}
