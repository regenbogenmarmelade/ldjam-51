using Godot;
using System;

public class MummyPath : PathFollow2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

    public override void _Process(float delta) {
   
        Offset += 200 * delta;
        var mummy = GetNode<Sprite>("Mummy1");
        mummy.FlipV = false; 
        if (mummy.GlobalPosition.x < 7500) {
            mummy.FlipH= true; 
        }
        if (mummy.GlobalPosition.x > 9300) {
            mummy.FlipH= false; 
        }
        
    }
}
