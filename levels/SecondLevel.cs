using Godot;
using System;

public class SecondLevel : Level
{
    
    public override void _Ready()
    {
        base._Ready();
    }
    
    private void PlayerEnteredBoat(object body) {

        if (body.GetType() == typeof(Player)) {
            var boatGroup = GetNode<Path2D>("BoatGroup");
            var path = boatGroup.GetNode<BoatPath>("BoatPath");
            path.GetNode<Timer>("Timer").Stop();
            path._startBoat = true;
        }
    }
    
    private void PlayerLeaveBoat(object body)
    {
        if (body.GetType() == typeof(Player)) {
            var boatGroup = GetNode<Path2D>("BoatGroup");
            var path = boatGroup.GetNode<BoatPath>("BoatPath");
            path._startBoat = false;
            path.GetNode<Timer>("Timer").Start();
        }
    }
    
    
    private void OnDebrisStarterBodyEntered(object body)
    {
        if (body.GetType() == typeof(Player)) {
            var debrisBowl = GetNode<StaticBody2D>("DebrisBowl");
            debrisBowl.GetNode<CollisionPolygon2D>("CollisionPolygon2D").QueueFree();
            GetNode<Area2D>("DebrisStarter").QueueFree();
        }
    }
    
    

    private void YokaiEntered(object body)
    {
        if (body is Player) {
          Player.Die();
        }
    }
}







