using Godot;
using System;
using System.ComponentModel.Design.Serialization;

public class ThirdLevel : Level {
    
    
    public override void _Ready()
    {
        base._Ready();
    }
    
    private void Plattform1Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform1").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform1").GetNode<Timer>("Timer").Start();
        }
    }
    
    private void Plattform2Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform2").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform2").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform3Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform3").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform3").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform4Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform4").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform4").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform5Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform5").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform5").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform6Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform6").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform6").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform7Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform7").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform7").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform8Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform8").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform8").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform9Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform9").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform9").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform10Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform10").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform10").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform11Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform11").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform11").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform12Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform12").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform12").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform13Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform13").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform13").GetNode<Timer>("Timer").Start();
        }
    }


    private void Plattform14Entered(object body)
    {
        if (body is Player) {
            GetNode<DesertPlattform>("Plattform14").GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
            GetNode<DesertPlattform>("Plattform14").GetNode<Timer>("Timer").Start();
        }
    }
    
    private void StartWorm(object body)
    {
        if (body is Player) {
            GetNode<Area2D>("DesertWorm").GetNode<AnimationPlayer>("AnimationPlayer").Play("appear");
        }
    }

    private void CollideWithWorm(object body) {
        if (body is Player) {
            Player.Die();
        }
    }
    
    private void CollideWithMummy(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }
    
    private void CactusEntered1(object body) {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered2(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered4(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered6(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered7(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered11(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered12(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered13(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered10(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered3(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered8(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered9(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }


    private void CactusEntered5(object body)
    {
        if (body is Player) {
            Player.Die();
        }
    }

}













