using Godot;
using System;

public class BoatPath : PathFollow2D {

    public bool _startBoat = false;
    public bool _isReachTimerStarted = false;


    // Called when the node enters the scene tree for the first time.
    public override void _Ready() {
    }

    public override void _Process(float delta) {
        if (_startBoat) {
            Offset += 300 * delta;
        }

        if (GlobalPosition.x > 17800 && !_isReachTimerStarted) {
            GetNode<AnimationPlayer>("AnimationPlayer").Play("ReachEndOfPath");
            _isReachTimerStarted = true;
            GetNode<Timer>("TimerReach").Start();
        }
    }
    
    private void OnTimeout() {
        _startBoat = false;
        GetNode<Timer>("Timer").Stop();
        GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeOut");
    }

    public void SetOffsetToStart() {
        Offset = 0;
        GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeIn");
        _isReachTimerStarted = false; 
    }

    private void OnTimerReachTimeout() {
        SetOffsetToStart();
        GetNode<Timer>("TimerReach").Stop();
        _isReachTimerStarted = false; 
    }

}
