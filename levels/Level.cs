using Godot;
using PlayerClass = Player;

public class Level : Node2D
{
    public Player Player => GetNode<Player>("Player");

    [Export]
    public int DeathFloor { get; set; } = 0;
    
    [Export]
    public int CameraYLimit = -320;

    public override void _Ready()
    {
        GameState.GetInstance(this).EmitSignal(nameof(GameState.LevelStarted), Name);
        
        var timer = GetNode<AnimationPlayer>("DisasterTimer");
        timer.Play("clock");
    }

    private void OnDisasterTimerTimeout()
    {
        GameState.GetInstance(this).StartNewGlobalDisaster(this);
    }
}
