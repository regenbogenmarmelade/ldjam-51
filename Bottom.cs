using Godot;

public class Bottom : ColorRect
{
    private void OnBodyEntered(object body)
    {
        if (body is Player player)
        {
            player.Die();
        }
    }
}
