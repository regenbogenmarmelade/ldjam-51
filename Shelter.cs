using System.Diagnostics;
using Godot;

public class Shelter : Area2D
{
    [Export] public bool Disabled { get; set; }

    private CollisionShape2D CollisionShape => GetNode<CollisionShape2D>("CollisionShape2D");
    
    public Vector2 Center => Position + CollisionShape.Position;

    private void OnBodyEntered(object body)
    {
        if (!(body is Player player))
        {
            return;
        }

        if (Disabled)
        {
            return;
        }

        Disabled = true;
        GetNode<Particles2D>("Particles2D").Emitting = false; 
        
        var sound = GetNode<AudioStreamPlayer2D>("Sound");
        if (!sound.Playing && !player.DeathSound.Playing)
        {
            sound.Play();
        }

        player.Invincible = true;
        player.LastShelter = Name;

        var level = GetParent<Level>();
        GameState.GetInstance(this).StopAllGlobalDisasters(level);
    }

    private void OnBodyExited(object body)
    {
        if (body is Player player)
        {
            player.Invincible = false;
        }
    }
}
