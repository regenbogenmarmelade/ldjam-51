using Godot;
using System;

public class Dragon : Area2D {

    private Timer _timer => GetNode<Timer>("Timer");

    private bool isAppeared = true; 
    
    public override void _Ready()
    {
        OnTimerTimeout();
       _timer.Start();
    }

    public void DragonAppear() {
        GetNode<AnimationPlayer>("AnimationPlayer").Play("appear");
        isAppeared = true;
    }

    public void DragonDisappear() {
        GetNode<AnimationPlayer>("AnimationPlayer").PlayBackwards("appear");
        isAppeared = false;
    }

    public void OnTimerTimeout() {
        if (isAppeared) {
            DragonDisappear();
        }
        else {
            DragonAppear();
        }
    }
    
    private void OnDragonBodyEntered(object body)
    {
        var level = GetParent<Level>();
        var player = level.GetNode<Player>("Player");

        if (body is Player) {
            player.Die();
        }
    }
}


