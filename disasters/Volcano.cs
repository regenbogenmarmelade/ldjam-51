using Godot;

public class Volcano : Area2D
{
    private bool _isErupting;

    private Particles2D Particles => GetNode<Particles2D>("Volcano_small/Particles2D");
    private CollisionShape2D CollisionShape => GetNode<CollisionShape2D>("CollisionShape2D");

    [Export]
    public bool IsErupting
    {
        get => _isErupting;
        set
        {
            _isErupting = value;
            Particles.Emitting = _isErupting;
            CollisionShape.Disabled = !_isErupting;
        }
    }

    public override void _Ready()
    {
        IsErupting = false;
    }

    private void OnBodyEntered(object body)
    {
        if (body is Player player)
        {
            player.Die();
        }
    }
}
