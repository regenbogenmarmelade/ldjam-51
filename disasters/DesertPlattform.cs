using Godot;
using System;

public class DesertPlattform : Area2D
{
    
    
    
    public override void _Ready() {
        GetNode<Sprite>("Plattform").Visible = true;
        GetNode<Sprite>("Plattform").Modulate = new Color(1,1,1,1);
        GetNode<CollisionShape2D>("Area2DShape").Disabled = false;
        var staticBody2D = GetNode<StaticBody2D>("StaticBody2D");
        staticBody2D.GetNode<CollisionShape2D>("CollisionShape2D").Disabled = false;
        
    }

    public void OnTimerTimeOut() {
        GetNode<AnimationPlayer>("AnimationPlayer").Play("FadeIn");
        GetNode<Timer>("Timer").Stop();
    }
    
}


