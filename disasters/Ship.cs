using Godot;
using Array = Godot.Collections.Array;

public class Ship : StaticBody2D
{
    private AnimationPlayer AnimationPlayer => GetNode<AnimationPlayer>("AnimationPlayer");

    public override void _Ready()
    {
        AnimationPlayer.Play("ShipGoes");
        GameState.GetInstance(this).Connect(
            nameof(GameState.DisasterStarted),
            AnimationPlayer,
            "play",
            new Array(new object[] { "ShipGoes" })
        );
    }
}
