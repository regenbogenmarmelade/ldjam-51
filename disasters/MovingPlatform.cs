using Godot;
using System;

public class MovingPlatform : Node2D
{
    private Vector2 _oldPos = Vector2.Zero;
    public Vector2 AnimatedVelocity{ get; private set; }

  public override void _Process(float delta)
  {
      AnimatedVelocity = GlobalPosition - _oldPos;
      _oldPos = GlobalPosition;
      //GD.Print("updating platform: " + this + " g:" + GlobalPosition + " o:" + _oldPos + " = " + AnimatedVelocity + " ");
  }
}
