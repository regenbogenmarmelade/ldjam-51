using System.Collections.Generic;
using System.Linq;
using Godot;

// TODO: should this be a global disaster?
public class Volcanism : Node2D
{
    private IEnumerable<Volcano> Volcanoes => GetChildren()
        .Cast<Object>()
        .AsQueryable()
        .Where(node => node is Volcano)
        .Cast<Volcano>();

    public override void _Ready()
    {
        GameState.GetInstance(this).Connect(
            nameof(GameState.DisasterStarted),
            this,
            nameof(Start)
        );
    }

    public void Start()
    {
        foreach (var volcano in Volcanoes)
        {
            volcano.IsErupting = true;
        }

        var stopTimer = GetNode<Timer>("StopEruptionTimer");
        stopTimer.Start();
    }

    private void Stop()
    {
        foreach (var volcano in Volcanoes)
        {
            volcano.IsErupting = false;
        }
    }
}
