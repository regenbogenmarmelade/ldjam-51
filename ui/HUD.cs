using System.Diagnostics;
using System.Linq;
using Godot;

public class HUD : CanvasLayer
{
    private AnimatedSprite NextDisasterIcon => GetNode<AnimatedSprite>("NextDisasterIcon");

    private Sprite ActiveDisasterBackground(int index) => GetNode<Sprite>($"ActiveDisasterBackground{index + 1}");
    private AnimatedSprite ActiveDisasterIcon(int index) => GetNode<AnimatedSprite>($"ActiveDisasterIcon{index + 1}");
    private TextureProgress DoubleJumpIcon => GetNode<TextureProgress>("DoubleJumpIcon");

    public override void _Ready()
    {
        UpdateIcons();
        var gameState = GameState.GetInstance(this);
        gameState.Connect(nameof(GameState.DisasterStarted), this, nameof(UpdateIcons));
        gameState.Connect(nameof(GameState.DisastersStopped), this, nameof(UpdateIcons));

        GetParent<Level>().Player.Connect(nameof(Player.DoubleJumpCooldownStarted), this, nameof(StartDoubleJumpCooldown));

        Debug.Assert(
            HasNode($"ActiveDisasterIcon{gameState.MaxNumberOfDisasters}"),
            $"Have you increased {nameof(gameState.MaxNumberOfDisasters)} without adding an icon?"
        );

        DoubleJumpIcon.Value = DoubleJumpIcon.MaxValue;
    }

    private void StartDoubleJumpCooldown(int seconds)
    {
        DoubleJumpIcon.MaxValue = 1000;
        DoubleJumpIcon.Value = 0f;
    }

    public override void _Process(float delta) {
        var _timer = GetParent<Level>().Player.DoubleJumpTimer;
        DoubleJumpIcon.Value = (_timer. WaitTime - _timer.TimeLeft) / _timer.WaitTime * 1000;
    }

    private void UpdateIcons()
    {
        var gameState = GameState.GetInstance(this);
        NextDisasterIcon.Animation = gameState.NextDisaster.Icon;

        for (var i = 0; i < gameState.ActiveDisasters.Count; i++)
        {
            var disaster = gameState.ActiveDisasters.ElementAt(i);
            ActiveDisasterIcon(i).Animation = disaster.Icon;
            ActiveDisasterBackground(i).Visible = true;
            ActiveDisasterIcon(i).Visible = true;
        }

        for (int i = gameState.ActiveDisasters.Count; i < gameState.MaxNumberOfDisasters; i++)
        {
            ActiveDisasterBackground(i).Visible = false;
            ActiveDisasterIcon(i).Visible = false;
        }
    }
}
