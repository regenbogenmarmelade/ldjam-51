using Godot;

public class StartScreen : Control
{
    private void StartFirstLevel()
    {
        GetTree().ChangeScene("levels/FirstLevel.tscn");
    }

    private void StartSecondLevel()
    {
        GetTree().ChangeScene("levels/SecondLevel.tscn");
    }


    private void StartThirdLevel()
    {
        GetTree().ChangeScene("levels/ThirdLevel.tscn");
    }  

    private void OnVolumeChanged(float value)
    {
        AudioServer.SetBusVolumeDb(0, 70 * (value - 100) / 100 );
    }
}
