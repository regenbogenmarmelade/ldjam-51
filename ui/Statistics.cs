using Godot;
using System;

public class Statistics : Control
{
    public override void _Ready()
    {
        var gameState = GameState.GetInstance(this);
        
        var now = DateTime.Now.Ticks;
        var levelDurationInTicks = now - gameState.LevelStartingTime;
        var levelDurationInMilliseconds = levelDurationInTicks / TimeSpan.TicksPerMillisecond;

        var richTextLabel = GetNode<RichTextLabel>("RichTextLabel");
        richTextLabel.BbcodeText =
            $"Finished level in [shake rate=5 level=10]{levelDurationInMilliseconds / 1000} seconds[/shake]\n" +
            $"Number of deaths: [shake rate=5 level=10]{gameState.NumberOfCasualties}[/shake]";
    }

    private void NavigateToStartScreen()
    {
        GetTree().ChangeScene("ui/StartScreen.tscn");
    }
}

