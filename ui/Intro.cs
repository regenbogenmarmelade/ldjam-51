using Godot;

public class Intro : Control
{
    public override void _Input(InputEvent @event)
    {
        if (!@event.IsPressed())
        {
            return;
        }

        var children = GetChildren();
        if (children.Count < 2)
        {
            GetTree().ChangeScene("ui/StartScreen.tscn");
            return;
        }

        var lastChild = (Node)children[children.Count - 1];
        lastChild.QueueFree();
    }
}
