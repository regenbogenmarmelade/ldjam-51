using Godot;

public class MeteorShower : Node2D
{
    private const int XRange = 500;

    private PackedScene MeteorScene => GD.Load<PackedScene>("global_disasters/Meteor.tscn");

    private void OnSpawnTimer()
    {
        var level = GetParent<Level>();
        var meteor = (Meteor) MeteorScene.Instance();
        var player = level.GetNode<Player>("Player");
        var screenHeight = GetViewportRect().Size.y;
        meteor.Position = new Vector2(player.Position) + new Vector2(GD.Randf() * XRange - XRange / 2.0f, -screenHeight);
        meteor.LinearVelocity = new Vector2(GD.Randf() * 300, 0);
        AddChild(meteor);
    }
}
