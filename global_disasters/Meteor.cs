using Godot;

public class Meteor : RigidBody2D
{
    public override void _Ready()
    {
        var scale = Vector2.One * (1.0f - GD.Randf() * 0.3f);
        var sprite = GetNode<Sprite>("Sprite");
        sprite.Scale = scale;
        var collisionShape = GetNode<CollisionShape2D>("CollisionShape2D");
        collisionShape.Scale = scale;
    }

    private void OnRockBodyEntered(object body)
    {
        if (body is Player player)
        {
            player.Die();
        }

        var sound = GetNode<AudioStreamPlayer2D>("Sound");
        sound.Play();
    }
}
