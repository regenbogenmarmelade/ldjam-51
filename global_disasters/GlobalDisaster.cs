using System.Collections.Generic;
using System.Linq;
using Godot;

public interface GlobalDisaster
{
    string Name { get; }
    string Icon { get; }
    void Start(Level level);
    void Stop(Level level);
}

public class AuraDisaster : GlobalDisaster
{
    private readonly string _auraName;

    public AuraDisaster(string auraName, string iconName)
    {
        _auraName = auraName;
        Icon = iconName;
    }

    public string Name => $"{nameof(AuraDisaster)}: {_auraName}";
    public string Icon { get; }

    public void Start(Level level)
    {
        var player = level.Player;
        player.Set(_auraName, true);
    }

    public void Stop(Level level)
    {
        var player = level.Player;
        player.Set(_auraName, false);
    }
}

public class SceneDisaster : GlobalDisaster
{
    private readonly string _scenePath;

    public SceneDisaster(string scenePath, string iconName)
    {
        _scenePath = scenePath;
        Icon = iconName;
    }

    public string Name => $"{nameof(SceneDisaster)}: {_scenePath}";
    public string Icon { get; }

    public void Start(Level level)
    {
        var sceneInstance = GD.Load<PackedScene>(_scenePath).Instance();
        sceneInstance.SetMeta(nameof(SceneDisaster), _scenePath);
        level.AddChild(sceneInstance);
    }

    public void Stop(Level level)
    {
        var node = GetSceneInstances(level).First();
        node.QueueFree();
    }

    private IEnumerable<Node> GetSceneInstances(Level level) => level.GetChildren()
        .Cast<Node>()
        .AsQueryable()
        .Where(node => (string)node.GetMeta(nameof(SceneDisaster), "") == _scenePath);
}
