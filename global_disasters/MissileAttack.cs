using Godot;

public class MissileAttack : Node2D
{
    private readonly PackedScene _missileScene = GD.Load<PackedScene>("global_disasters/Missile.tscn");

    public override void _Ready()
    {
        GameState.GetInstance(this).Connect(
            nameof(GameState.DisasterStarted),
            this,
            nameof(Start)
        );
    }

    public void Start()
    {
        var missile = (Missile) _missileScene.Instance();
        var level = GetParent<Level>();
        var offset = level.Player.Position + new Vector2(1000, -300); // TODO: remove hardcoded values
        AddChild(missile);
        missile.Launch(offset);
    }
}
