using Godot;

public class Missile : RigidBody2D
{
    public void Launch(Vector2 offset)
    {
        LinearVelocity = Vector2.Left * 600;
        Position = offset + new Vector2(GD.Randf() * 400, (GD.Randf() - 0.5f) * 400);
        var sound = GetNode<AudioStreamPlayer2D>("Sound");
        sound.Play();
    }

    public override void _IntegrateForces(Physics2DDirectBodyState state)
    {
        var sprite = GetNode<Sprite>("Sprite");
        var width = sprite.Texture.GetWidth();
        var position = state.Transform.origin;
        if (position.x + width < 0)
        {
            QueueFree();
        }
    }

    private void OnBodyEntered(object body)
    {
        if (body is Player player)
        {
            player.Die();
        }

        QueueFree();
    }
}
