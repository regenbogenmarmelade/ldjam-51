using System.Diagnostics;
using System.Linq;
using Godot;

public class Player : RigidBody2D
{
    // basic physics consants
    private const int HorizontalVelocityFactor = 350;
    private const int JumpVelocityFactor = 500;

    private const int CoyoteFrames = 10;
    private const int JumpAscendFrames = 20;

    private const int MaxJumpCharges = 1;
    private const int MaxVelocity = 1500;
    private const int DoubleJumpRechargeTime = 5;

    // disaster constants
    private const float WindStrength = 0.5f;
    private const float SlipperyFriction = 0.1f;
    private const float StickyMultiplier = 0.5f;
    private const float StandardGravity = 15f;
    private const float HighGravity = 40f;
    private const float LowGravity = 5f;

    private AnimatedSprite _animatedSprite => GetNode<AnimatedSprite>("AnimatedSprite");
    public Timer DoubleJumpTimer => GetNode<Timer>("DoubleJumpTimer");
    public AudioStreamPlayer2D DeathSound => GetNode<AudioStreamPlayer2D>("DeathSound");

    private Camera2D camera;

    // state engine stuff
    [Export] private byte FramesJumping = 0;
    [Export] private byte FramesSinceGround = 0;
    [Export] private byte ExtraJumpCharges = 1;
    [Export] private bool IsOnGround = false;
    [Export] private bool TouchWallLeft = false;
    [Export] private bool TouchWallRight = false;

    [Export] public bool Invincible
    {
        get => GetCollisionLayerBit((int)CollisionLayers.Disasters);
        set => SetCollisionLayerBit((int)CollisionLayers.Disasters, !value);
    }

    [Export] public string LastShelter { get; set; }


    [Export] public bool AuraWindLeft { get; set; } = false;
    [Export] public bool AuraWindRight { get; set; } = false;
    [Export] public bool AuraInvertedControls { get; set; } = false;
    [Export] public bool AuraSlippery {
        get => PhysicsMaterialOverride.Friction == SlipperyFriction;
        set => PhysicsMaterialOverride.Friction = value ? SlipperyFriction : 1f;
    }
    [Export] public bool AuraSticky { get; set; }
    [Export] public bool AuraHighGravity {
        get => GravityScale == HighGravity;
        set => GravityScale = value ? HighGravity : StandardGravity;
    }
    [Export] public bool AuraLowGravity {
        get => GravityScale == LowGravity;
        set => GravityScale = value ? LowGravity : StandardGravity;
    }

    [Signal]
    public delegate void DoubleJumpCooldownStarted(int seconds);

    private static bool IsJumpPressed => Input.IsActionPressed("ui_up");

    private enum PlayerState
    {
        RESPAWN,    // player should respawn at the last checkpoint. takes precedence over every other state
        GROUND,     // player is touching ground. resets all jumps
        JUMPING,    // player is currently in a jump and hasn't let go of the button yet
        FALLING,    // no jump pressed, and not currently on ground
        AFTER_JUMP, // player is still pressing jump after the jump frames have run out
        PRE_JUMP    // player is trying to press jump while falling without jump charges. only transitions to ground once jumpis being let go of
    }

    [Export] private PlayerState PState = PlayerState.GROUND;

    public override void _Ready()
    {
        GameState.GetInstance(this).Connect(nameof(GameState.LevelStarted), this, nameof(OnLevelEntered));
        camera = (Camera2D)GetNode("Camera2D");
    }

    public void OnLevelEntered(string levelName)
    {
        var level = GetParent<Level>();
        camera.LimitTop = level.CameraYLimit;
        var firstShelter = level.GetChildren()
            .Cast<Object>()
            .AsQueryable()
            .Where(node => node is Shelter)
            .Cast<Shelter>()
            .First();

        Reset();
        LastShelter = firstShelter.Name;
    }

    public override void _Process(float delta)
    {
        if (IsOnGround)
        {
            if (!Input.IsActionPressed("ui_left") && !Input.IsActionPressed("ui_right"))
            {
                _animatedSprite.Play("idle");
            }
            else
            {
                _animatedSprite.FlipH = Input.IsActionPressed("ui_right");
                _animatedSprite.Play("run");
            }
        }
        else
        {
            _animatedSprite.FlipH = Input.IsActionPressed("ui_right");
            _animatedSprite.Play("jump");
        }

    }

    public override void _IntegrateForces(Physics2DDirectBodyState state)
    {
        if (PState == PlayerState.RESPAWN) {
            DoRespawn(state);
            return;
        }

        if (LinearVelocity.Abs().x > MaxVelocity || LinearVelocity.Abs().y > MaxVelocity)
        {
            Vector2 newSpeed = LinearVelocity.Normalized();
            newSpeed *= MaxVelocity;
            LinearVelocity = newSpeed;
        }

        IsOnGround = false;
        TouchWallLeft = false;
        TouchWallRight = false;
        for (int i = 0; i < state.GetContactCount(); i++)
        {
            var CollisionDirection = state.GetContactColliderPosition(i) - GlobalPosition;
            IsOnGround     |= CollisionDirection.y > 0 && Mathf.Abs(CollisionDirection.y) > Mathf.Abs(CollisionDirection.x);
            TouchWallLeft  |= CollisionDirection.x < 0 && Mathf.Abs(CollisionDirection.x) > Mathf.Abs(CollisionDirection.y);
            TouchWallRight |= CollisionDirection.x > 0 && Mathf.Abs(CollisionDirection.x) > Mathf.Abs(CollisionDirection.y);
        }


        var NextState = PState;

        // process inputs
        var JoystickStrength = Input.GetActionStrength("ui_left") - Input.GetActionStrength("ui_right");
        var DigiInput = (Input.IsActionPressed("ui_left") ? -1 : 0) + (Input.IsActionPressed("ui_right") ? 1 : 0);
        var CombinedInput = JoystickStrength != 0 ? JoystickStrength : DigiInput;
        var _effectiveInput =
            (AuraInvertedControls ? -1 : 1)
            * (IsOnGround && AuraSticky ? StickyMultiplier : 1)
            * CombinedInput
            + (AuraWindLeft  ? - WindStrength : 0)
            + (AuraWindRight ?   WindStrength : 0);

        if (_effectiveInput > 0 && !TouchWallLeft)
        {
            LinearVelocity = new Vector2((Vector2.Left * HorizontalVelocityFactor * _effectiveInput).x, LinearVelocity.y);
        }

        if (_effectiveInput < 0 && !TouchWallRight)
        {
            LinearVelocity = new Vector2((Vector2.Left * HorizontalVelocityFactor * _effectiveInput).x, LinearVelocity.y);
        }

        switch(PState)
        {
            case PlayerState.GROUND:
                FramesSinceGround = 0;
                FramesJumping = 0;
                //ExtraJumpCharges = MaxJumpCharges;

                if (IsJumpPressed)
                {
                    StartJump();
                    ApplyJump();
                    NextState = PlayerState.JUMPING;
                }
                break;

            case PlayerState.JUMPING:
                IncFramesSinceGround();
                IncFramesJumping();

                if (IsJumpPressed)
                {
                    if (FramesJumping > JumpAscendFrames) // reached end of jump rising
                    {
                        NextState = PlayerState.AFTER_JUMP;
                    }
                    else // continue rising
                    {
                        ApplyJump();
                    }
                } else {
                    NextState =  PlayerState.FALLING;
                }

                break;

            case PlayerState.AFTER_JUMP:
                if (!IsJumpPressed)
                {
                    NextState = PlayerState.FALLING;
                }
                break;

            case PlayerState.PRE_JUMP:
                if (!IsJumpPressed)
                {
                    NextState = PlayerState.FALLING;
                }
                break;

            case PlayerState.FALLING:
                IncFramesSinceGround();
                FramesJumping = 0;

                if (IsOnGround)
                {
                    var sound = GetNode<AudioStreamPlayer2D>("GroundSound");
                    if (!sound.Playing)
                    {
                        sound.Play();
                    }
                    NextState = PlayerState.GROUND;
                    goto case PlayerState.GROUND;
                }

                if (IsJumpPressed)
                {
                    // have extra jumps? consume those first
                    if (ExtraJumpCharges > 0) {
                        ExtraJumpCharges--;
                        StartJump();
                        ApplyJump();
                        DoubleJumpTimer.Start(DoubleJumpRechargeTime);
                        EmitSignal(nameof(DoubleJumpCooldownStarted), DoubleJumpRechargeTime);
                        NextState =  PlayerState.JUMPING;
                        break;
                    }
                    // no extra jumps? check if coyote time
                    if (FramesSinceGround < CoyoteFrames)
                    {
                        StartJump();
                        ApplyJump();
                        NextState =  PlayerState.JUMPING;
                        break;
                    }

                    // still pressing jump? to pres_jump with you
                    NextState = PlayerState.PRE_JUMP;
                }

                break;
        }

        // add moving platform forces
        for (int i = 0; i < state.GetContactCount(); i++)
        {
            if (!IsOnGround)
            {
                continue;
            }

            var collisionObject = state.GetContactColliderObject(i);
            switch (collisionObject)
            {
                case MovingPlatform movingPlatform:
                {
                    // add momentum of the platform to the player if the collision is below, adjusted for mass by feelycraft.
                    var movement = movingPlatform.AnimatedVelocity;
                    ApplyCentralImpulse(movement * 15);
                    break;
                }
                case StaticBody2D staticBody when staticBody.Name.StartsWith("BounceShroom"):
                {
                    var bounceSound = GetNode<AudioStreamPlayer2D>("BounceSound");
                    if (!bounceSound.Playing)
                    {
                        bounceSound.Play();
                    }

                    break;
                }
            }
        }

        // check for death floor of level
        var level = GetParent<Level>();
        if (GlobalPosition.y > level.DeathFloor)
        {
            Die();
            return;
        }

        PState = NextState;
    }

    private void StartJump()
    {
        var sound = GetNode<AudioStreamPlayer2D>("JumpSound");
        if (!sound.Playing)
        {
            sound.Play();
        }
    }

    private void ApplyJump()
    {
        LinearVelocity = new Vector2(LinearVelocity.x, (Vector2.Up * JumpVelocityFactor).y);
    }

    private void IncFramesJumping()
    {
        FramesJumping = (byte)Mathf.Min(FramesJumping + 1, 254);
    }

    private void IncFramesSinceGround()
    {
        FramesSinceGround = (byte)Mathf.Min(FramesSinceGround + 1, 254);
    }

    public void Die()
    {
        PState = PlayerState.RESPAWN;

        var gameState = GameState.GetInstance(this);
        gameState.EmitSignal(nameof(GameState.PlayerDied));

        // TODO: if stopping disasters is possible without Level instance, this can be moved to GameState
        var level = GetParent<Level>();
        gameState.StopAllGlobalDisasters(level);

        if (!DeathSound.Playing)
        {
            DeathSound.Play();
        }
    }

    public void DoRespawn(Physics2DDirectBodyState state)
    {
        var level = GetParent<Level>();
        var lastShelter = level.GetNode<Shelter>(LastShelter);
        var transform = state.Transform;
        transform.origin = lastShelter.Center;
        state.AngularVelocity = 0;
        state.LinearVelocity = Vector2.Zero;
        state.Transform = transform;

        FramesSinceGround = 0;
        FramesJumping = 0;

        PState = IsJumpPressed ? PlayerState.PRE_JUMP : PlayerState.GROUND;
    }

    public void Reset()
    {
        PState = PlayerState.RESPAWN;
    }

    private void RechargeJumps()
    {
        GD.Print("recharging jump");
        if (ExtraJumpCharges < MaxJumpCharges)
        {
            ExtraJumpCharges += 1;
        }
    }
}
