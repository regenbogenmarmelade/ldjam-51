using System;
using System.Collections.Generic;
using System.Diagnostics;
using Godot;

public class GameState : Node
{
    [Signal]
    public delegate void DisasterStarted();

    [Signal]
    public delegate void DisastersStopped();

    [Signal]
    public delegate void LevelStarted(string levelName);

    [Signal]
    public delegate void PlayerDied();

    public static GameState GetInstance(Node node)
    {
        return node.GetNode<GameState>($"/root/{nameof(GameState)}");
    }

    [Export] public uint MaxNumberOfDisasters { get; set; } = 5;

    public Queue<GlobalDisaster> ActiveDisasters { get; } = new Queue<GlobalDisaster>();

    public GlobalDisaster NextDisaster { get; set; }

    private static readonly GlobalDisaster[] AvailableDisasters =
    {
        new AuraDisaster(nameof(Player.AuraHighGravity), "gravity_high"),
        new AuraDisaster(nameof(Player.AuraInvertedControls), "confusion"),
        new AuraDisaster(nameof(Player.AuraLowGravity), "gravity_low"),
        new AuraDisaster(nameof(Player.AuraSlippery), "ice"),
        new AuraDisaster(nameof(Player.AuraSticky), "sticky"),
        new AuraDisaster(nameof(Player.AuraWindLeft), "tornado_left"),
        new AuraDisaster(nameof(Player.AuraWindRight), "tornado_right"),
        new SceneDisaster("global_disasters/MeteorShower.tscn", "meteor"),
        new SceneDisaster("global_disasters/MissileAttack.tscn", "missile")
    };

    public long LevelStartingTime { get; private set; }
    public int NumberOfCasualties { get; private set; }

    public override void _Ready()
    {
        GD.Randomize();
        Connect(nameof(LevelStarted), this, nameof(OnLevelStarted));
        Connect(nameof(PlayerDied), this, nameof(IncreasePlayerDeathCount));
        DetermineNextDisaster();
    }

    private void DetermineNextDisaster()
    {
        GlobalDisaster globalDisaster;
        do
        {
            var disasterIndex = (int)Math.Floor(GD.RandRange(0, AvailableDisasters.Length));
            globalDisaster = AvailableDisasters[disasterIndex];
        } while (globalDisaster is AuraDisaster && ActiveDisasters.Contains(globalDisaster));

        NextDisaster = globalDisaster;
    }

    public void OnLevelStarted(string levelName)
    {
        var now = DateTime.Now.Ticks;
        LevelStartingTime = now;
        NumberOfCasualties = 0;
    }

    public void IncreasePlayerDeathCount()
    {
        NumberOfCasualties++;
    }

    public void StartNewGlobalDisaster(Level level)
    {
        if (ActiveDisasters.Count >= MaxNumberOfDisasters)
        {
            var oldestDisaster = ActiveDisasters.Dequeue();
            Debug.Print($"Too many active disasters already, removing {oldestDisaster.Name}");
            oldestDisaster.Stop(level);
        }

        Debug.Assert(NextDisaster != null, $"{nameof(NextDisaster)} is not set!");
        Debug.Print($"Starting global disaster {NextDisaster.Name}");
        NextDisaster.Start(level);
        ActiveDisasters.Enqueue(NextDisaster);
        DetermineNextDisaster();

        EmitSignal(nameof(DisasterStarted));
    }

    public void StopAllGlobalDisasters(Level level)
    {
        foreach (var globalDisaster in ActiveDisasters)
        {
            globalDisaster.Stop(level);
        }

        ActiveDisasters.Clear();

        EmitSignal(nameof(DisastersStopped));
    }
}
